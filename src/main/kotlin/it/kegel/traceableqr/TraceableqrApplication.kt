package it.kegel.traceableqr

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TraceableqrApplication

fun main(args: Array<String>) {
	runApplication<TraceableqrApplication>(*args)
}
