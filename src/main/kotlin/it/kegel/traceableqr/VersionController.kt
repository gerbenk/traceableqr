package it.kegel.traceableqr

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class VersionController {

    @GetMapping("/version")
    fun getVersion() = "0.1"
}