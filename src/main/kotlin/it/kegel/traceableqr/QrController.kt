package it.kegel.traceableqr

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URL
import javax.servlet.http.HttpServletRequest

@RestController
class QrController {

    @GetMapping("/**")
    fun scan(request: HttpServletRequest, @RequestParam id: String?, @RequestParam src: String?): ResponseEntity<String> {
        val url = URL("https://" + request.requestURI.substring(1))
        val ip = request.getClientIp()

        println("${id ?: "?"}\t$ip\t${request.remoteHost}\t${src ?: "/"}\t$url")
        return ResponseEntity.status(302).header("Location", url.toString()).build()
    }

    fun HttpServletRequest.getClientIp() =
        getHeader("X-FORWARDED-FOR") ?: remoteAddr

}