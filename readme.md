## Traceable QR

Sometimes, you would like to know who is scanning your QR codes. This simple service makes it possible to track that.

Generate a QR code which points to https://qr.kegel.it/www.cnn.com When someone scans the URL, it he/she will visit this service, you make a lock of teh request and forward the user to the requested URL.

